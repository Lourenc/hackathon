angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
    }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  },

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };

})
.controller('booksCtrl', function($scope) {
  $scope.data = {};

  setInterval(function() {
    $scope.books = Books;
        $scope.$apply() 
    }, 500);

  $scope.clearSearch = function() {
    $scope.data.searchQuery = '';
  };
})
.controller('bookCtrl',function($scope, $stateParams, $location){
  for(var b = 0; b < Books.length; b++)
    if (Books[b]._id === $stateParams.bookId)
      $scope.book = Books[b];

  $scope.addToFave = function(book){
  var fave = window.localStorage.getObj('fave');
  if(!fave || fave==null)
    fave = new Array();
  fave.push(book);
  console.log(fave);
  for(var i = 0; i< fave.length-1; i++)
    for(var j=i+1; j < fave.length; j++)
      if(fave[i]._id==fave[j]._id)
        fave.splice(i,1);
  window.localStorage.setObj('fave',fave) ;
  console.log(window.localStorage.getObj('fave'));
  // localStorage.removeItem('fave');
  // localStorage.removeItem('favourites');
  }
})
.controller('favouritesCtrl', function($scope){
  setInterval(function() {
    $scope.books = window.localStorage.getObj('fave');
        $scope.$apply() 
    }, 500);
});
