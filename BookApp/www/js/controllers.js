angular.module('starter.controllers', []).factory('AuthenticationService', function() {
    var auth = {
        isAuthenticated: false,
        isAdmin: false
    }
    return auth;
}).factory('TokenInterceptor', function($q, $window, $location, AuthenticationService) {
    return {
        request: function(config) {
            config.headers = config.headers || {};
            if ($window.sessionStorage.token) {
                config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
            }
            return config;
        },
        requestError: function(rejection) {
            return $q.reject(rejection);
        },
        /* Set Authentication.isAuthenticated to true if 200 received */
        response: function(response) {
            if (response != null && response.status == 200 && $window.sessionStorage.token && !AuthenticationService.isAuthenticated) {
                AuthenticationService.isAuthenticated = true;
            }
            return response || $q.when(response);
        },
        /* Revoke client authentication if 401 is received */
        responseError: function(rejection) {
            if (rejection != null && rejection.status === 401 && ($window.sessionStorage.token || AuthenticationService.isAuthenticated)) {
                delete $window.sessionStorage.token;
                AuthenticationService.isAuthenticated = false;
                $location.path("/admin/login");
            }
            return $q.reject(rejection);
        }
    };
}).factory('UserService', function($http) {
    return {
        signIn: function(email, password) {
            return $http.post(options.api.base_url + '/user/signin', {
                email: email,
                password: password
            });
        },
        logOut: function() {
            return $http.get(options.api.base_url + '/user/logout');
        },
        register: function(email, username, password) {
            return $http.post(options.api.base_url + '/user/register', {
                email: email,
                username: username,
                password: password
            });
        }
    }
})
.controller('AppCtrl', function($scope, $ionicModal,$http, $timeout, $location, $window, UserService, AuthenticationService) {
    // Form data for the login modal
    $scope.loginData = {};
    $scope.registerData = {};
    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $ionicModal.fromTemplateUrl('templates/register.html', {
        scope: $scope
    }).then(function(modal1) {
        $scope.modal1 = modal1;
    });

    // Triggered in the login modal to close it
    $scope.closeRegister = function() {
        $scope.modal1.hide();
    },
    $scope.closeLogin = function() {
        $scope.modal.hide();
    },
    // Open the login modal
    $scope.login = function() {
        $scope.modal.show();
    };
    $scope.registerShow = function() {
        $scope.modal1.show();
    };
    $scope.doRegister = function(){
        console.log($scope.registerData);
        $scope.register($scope.registerData.email,$scope.registerData.username,$scope.registerData.password);
        $scope.modal1.hide();
    }
    $scope.doLogin = function() {
        console.log('hiih');
        $scope.signIn($scope.loginData.email, $scope.loginData.password);
        $scope.modal.hide();
    };
    

    $scope.signIn = function signIn(username, password) {
        if (username != null && password != null) {
            UserService.signIn(username, password).success(function(data) {
                AuthenticationService.isAuthenticated = true;
                $window.sessionStorage.token = data.token;
                $location.path("/books");
            }).error(function(status, data) {
                console.log(status);
                console.log(data);
            });
        }
    }
    $scope.logOut = function logOut() {
        if (AuthenticationService.isAuthenticated) {
            UserService.logOut().success(function(data) {
                AuthenticationService.isAuthenticated = false;
                delete $window.sessionStorage.token;
                $location.path("/");
            }).error(function(status, data) {
                console.log(status);
                console.log(data);
            });
        } else {
            $location.path("/login");
        }
    }
    $scope.register = function register(email, username, password) {
        console.log(email);
        if (AuthenticationService.isAuthenticated) {
            $location.path("/books");
        } else {
            UserService.register(email, username, password).success(function(data) {
                $location.path("/login");
            }).error(function(status, data) {
                console.log(status);
                console.log(data);
            });
        }
    }
}).controller('booksCtrl', function($scope) {
    $scope.data = {};
    setInterval(function() {
        $scope.books = Books;
        $scope.$apply()
    }, 500);
    $scope.clearSearch = function() {
        $scope.data.searchQuery = '';
    };
}).controller('bookCtrl', function($scope, $stateParams, $location) {
    for (var b = 0; b < Books.length; b++)
        if (Books[b]._id === $stateParams.bookId) $scope.book = Books[b];
    $scope.addToFave = function(book) {
        var fave = window.localStorage.getObj('fave');
        if (!fave || fave == null) fave = new Array();
        fave.push(book);
        console.log(fave);
        for (var i = 0; i < fave.length - 1; i++)
            for (var j = i + 1; j < fave.length; j++)
                if (fave[i]._id == fave[j]._id) fave.splice(i, 1);
        window.localStorage.setObj('fave', fave);
        console.log(window.localStorage.getObj('fave'));
        // localStorage.removeItem('fave');
        // localStorage.removeItem('favourites');
    }
}).controller('favouritesCtrl', function($scope) {
    setInterval(function() {
        $scope.books = window.localStorage.getObj('fave');
        $scope.$apply()
    }, 500);
}).controller('configCtrl', function($scope) {

});
