// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var Books;
Storage.prototype.setObj = function(key, obj) {
    return this.setItem(key, JSON.stringify(obj))
}
Storage.prototype.getObj = function(key) {
    return JSON.parse(this.getItem(key))
}

$.getJSON('http://localhost:3000/book', function(data) {
    Books = data;
  });

var options = {};
options.api = {};
options.api.base_url = "http://localhost:3000";

angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

  });
})
.config(function ($httpProvider) {
    $httpProvider.interceptors.push('TokenInterceptor');
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'AppCtrl'
    })

    .state('app.search', {
      url: "/search",
      views: {
        'menuContent' :{
          templateUrl: "templates/search.html"
        }
      }
    })

    .state('app.browse', {
      url: "/browse",
      views: {
        'menuContent' :{
          templateUrl: "templates/browse.html"
        }
      }
    })
    .state('app.books', {
      url: "/books",
      views: {
        'menuContent' :{
          templateUrl: "templates/books.html",
          controller: 'booksCtrl'
        }
      }
    })
    .state('app.favourites', {
      url: "/favourites",
      views: {
        'menuContent' :{
          templateUrl: "templates/favourites.html",
          controller: 'favouritesCtrl'
        }
      }
    })
    .state('app.config', {
      url: "/config",
      views: {
        'menuContent' :{
          templateUrl: "templates/config.html",
          controller: 'configCtrl'
        }
      }
    })

    .state('app.single', {
      url: "/books/:bookId",
      views: {
        'menuContent' :{
          templateUrl: "templates/book.html",
          controller: 'bookCtrl'
        }
      }
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/books');
});
