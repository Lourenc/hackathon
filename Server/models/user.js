var bcrypt = require('bcrypt');
var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , crypto = require('crypto')
    , oAuthTypes = ['github', 'twitter', 'facebook', 'google', 'linkedin'];

var User = new Schema({
    email: { type: String, required: true, unique: true },
    username: {type: String, required : true, unique: true },
    password: { type: String, required: true},
    favourites: { type: Array  }

});
// Bcrypt middleware on UserSchema
User.pre('save', function(next) {
  var user = this;

  if (!user.isModified('password')) return next();

  bcrypt.genSalt(10, function(err, salt) {
    if (err) return next(err);

    bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) return next(err);
        user.password = hash;
        next();
    });
  });
});

//Password verification
User.methods.comparePassword = function(password, cb) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(isMatch);
    });
};
mongoose.model('User', User);

exports.User = function (db) {
    return db.model('User');
};
