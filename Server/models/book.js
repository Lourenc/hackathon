var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , crypto = require('crypto')
    , oAuthTypes = ['github', 'twitter', 'facebook', 'google', 'linkedin'];

var BookSchema = new Schema({
    name: {type: String, default: ''},
    author: {type: String, default: ''},
    tags: {type: String, default: ''},
    text: {type: String, default: ''},
    category: {type: String, default: ''},
    date: {type: Date, default: null},
    url: {type: String, default: ''},
    like: {type: Number, default: 0}
});

mongoose.model('Book', BookSchema);

exports.Book = function (db) {
    return db.model('Book');
};
