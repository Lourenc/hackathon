var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var secret = require('../config/secret');
// var redisClient = require('../config/redis_database').redisClient;
var tokenManager = require('../config/token_manager');

var db = mongoose.connection;


var User = require('../models/user.js').User(db);


exports.signin = function(req, res) {
	var email = req.body.email || '';
	var password = req.body.password || '';

	if (email == '' || password == '') {
		return res.send(401);
	}

	User.findOne({email: email}, function (err, user) {
		if (err) {
			console.log(err);
			return res.send(401);
		}

		if (user == undefined) {
			return res.send(401);
		}

		user.comparePassword(password, function(isMatch) {
			if (!isMatch) {
				console.log("Attempt failed to login with " + user.email);
				return res.send(401);
            }

			var token = jwt.sign({id: user._id}, secret.secretToken, { expiresInMinutes: tokenManager.TOKEN_EXPIRATION });

			return res.json({token:token});
		});

	});
};

exports.logout = function(req, res) {
	if (req.user) {
		tokenManager.expireToken(req.headers);

		delete req.user;
		return res.send(200);
	}
	else {
		return res.send(401);
	}
}

exports.register = function(req, res) {
	console.log(req.body);
	var email = req.body.email || '';
	var password = req.body.password || '';
	var username = req.body.username || '';


	if (email == '' || password == '' || username == '') {
		return res.send(400);
	}

	var user = new User();
	user.email = email;
	user.username = username;
	user.password = password;

	user.save(function(err) {
		if (err) {
			console.log(err);
			return res.send(500);
		}

		User.count(function(err, counter) {
			if (err) {
				console.log(err);
				return res.send(500);
			}

			if (counter == 1) {
				User.update({email:user.email}, {is_admin:true}, function(err, nbRow) {
					if (err) {
						console.log(err);
						return res.send(500);
					}

					console.log('First user created as an Admin');
					return res.send(200);
				});
			}
			else {
				return res.send(200);
			}
		});
	});
}
