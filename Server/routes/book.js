var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var secret = require('../config/secret');
// var redisClient = require('../config/redis_database').redisClient;
var tokenManager = require('../config/token_manager');

var db = mongoose.connection;


var Book = require('../models/book.js').Book(db);

exports.get_books = function(req, res){
  Book.find().limit(50).sort('asc').exec(function(err, books){
    if(err)
      console.log(err);
    else{
      res.header("Access-Control-Allow-Origin", "*");
      res.json(books);
    }
  });
};
exports.post_book = function(req, res){
  var _book = new Book(req.body);
  _book.date = Date.now();

  _book.save(function (err) {
        if (err) {
            return res.redirect('/error');
        } else return res.redirect('/');
    });
};
