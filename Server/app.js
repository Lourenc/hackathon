var express = require('express');
var http = require('http');
var path = require('path');
var app = express();
var jwt = require('express-jwt');
var tokenManager = require('./config/token_manager');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var secret = require('./config/secret');
var colors = require('colors');

mongoose.connect('mongodb://user:abracadabra@kahana.mongohq.com:10086/hackathon_book');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
    console.log('√ Database connected!'.green);
});
app.configure(function() {
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.json());
  app.use(express.urlencoded());
  app.use(express.bodyParser());

  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});



var routes = {};
routes.users = require('./routes/user.js');
routes.books = require('./routes/book.js');


var Book = require('./models/book.js').Book(db);
var User = require('./models/user.js').User(db);


////////////Access control/////////////

app.all('*', function(req, res, next) {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Credentials', true);
  res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
  res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
  if ('OPTIONS' == req.method) return res.send(200);
  next();
});

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}


//////////ROUTES//////////

app.post('/user/register', routes.users.register);

app.post('/user/signin', routes.users.signin);

app.get('/user/logout', jwt({secret: secret.secretToken}), routes.users.logout);

app.get('/book', routes.books.get_books);

app.post('/book', routes.books.post_book);




app.get('/', function(req,res){
  res.render('index');
});

app.get('/error', function(req,res){
  res.render('error.ejs');
});

http.createServer(app).listen(app.get('port'), function(){
  console.log('√ Server is running on port: '.green + app.get('port').toString().green);
});
